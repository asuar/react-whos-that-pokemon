# Licenses

All images © Nintendo.

# React Who's That Pokemon?

This game shows you a random Pokemon's silhouette and you have to guess which Pokemon it is.
This project uses Axios to search [PokeAPI](https://pokeapi.co/) for Pokemon information and sprites.

## Features :star2:

:star: Cache Pokemon data\
:star: Debounce API calls\
:star: Random Pokemon silhouette\
:star: Original 151 Pokemon\
:star: Replay

## Additional Work :hammer:

🛠️ Add a "Next Pokemon" button to skip the current Pokemon\
🛠️ Add responsive design

### Technologies used 🛠️

- [React](https://es.reactjs.org/) - Front-End JavaScript library
- [Axios](https://www.npmjs.com/package/axios) - Promise based HTTP client for node.js

## Try out my code 🚀

This code can be deployed directly on a service such as [Netlify](https://netlify.com). Here is how to install it locally:

### Prerequisites :clipboard:

[Git](https://git-scm.com)\
[NPM](http://npmjs.com)\
[React](https://es.reactjs.org/)

### Setup :wrench:

From the command line, first clone react-tetris:

```bash
# Clone this repository
$ git clone https://gitlab.com/asuar/react-search.git

# Go into the repository
$ cd react-search

# Remove current origin repository
$ git remote remove origin
```

### Install with NPM

```bash
# Install dependencies
$ npm install

# Start development server
$ npm start
```

Once the server has started, the site can be accessed at `http://localhost:3000/`
