import { useEffect, useState } from 'react';
import axios from 'axios';
import './App.css';
import pokeball from './images/pokeball.png';

const MAX_POKEDEX = 151;
const DEBOUNCE_TIME = 200;
let debounceTimer;
const POKE_API_URL = `https://pokeapi.co/api/v2/pokemon?limit=${MAX_POKEDEX}`;
const POKE_IMAGE_API_URL = `https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/`;

function App() {
  const [error, setError] = useState(null);
  const [loading, setLoading] = useState(false);
  const [random4Pokemon, setRandom4Pokemon] = useState([]);
  const [pokemonList, setPokemonList] = useState([]);
  const [chosenPokemon, setChosenPokemon] = useState(null);
  const [chosenPokemonImage, setChosenPokemonImage] = useState(null);
  const [isGameOver, setIsGameOver] = useState(false);
  const [gameWon, setGameWon] = useState(false);

  const fetchPokemon = () => {
    axios
      .get(POKE_API_URL)
      .then((response) => {
        const pokemonList = response.data.results.map((pokemon, index) => ({
          ...pokemon,
          id: index + 1,
        }));
        setPokemonList([...pokemonList]);
        let random4Pokemon = get4randomPokemon(pokemonList);
        setRandom4Pokemon(random4Pokemon);
        choosePokemon(random4Pokemon);
        setLoading(false);
      })
      .catch((error) => {
        setError(error);
        setLoading(false);
      });
  };

  const choosePokemon = (pokemon = []) => {
    let pickedPokemon = [...pokemon].sort(() => 0.5 - Math.random())[0];
    setChosenPokemon(pickedPokemon);
    setChosenPokemonImage(`${POKE_IMAGE_API_URL}${pickedPokemon.id}.png`);
  };

  const handleReset = () => {
    if (debounceTimer) {
      clearTimeout(debounceTimer);
    }

    setIsGameOver(false);
    setGameWon(false);

    if (pokemonList.length === 0) {
      setLoading(true);
      setRandom4Pokemon([]);
      debounceTimer = setTimeout(() => {
        fetchPokemon();
      }, DEBOUNCE_TIME);
    } else {
      if (pokemonList.length > 0) {
        let random4Pokemon = get4randomPokemon(pokemonList);
        setRandom4Pokemon(random4Pokemon);
        choosePokemon(random4Pokemon);
      }
    }
  };

  const get4randomPokemon = (pokemonList) => {
    return [...pokemonList]
      .sort(() => 0.5 - Math.random())
      .slice(0, 4)
      .map((pokemon) => {
        return { id: pokemon.id, name: pokemon.name };
      });
  };

  const handlePokemonSelect = (e) => {
    let selectedPokemon = e.target.innerHTML;
    if (selectedPokemon.toLowerCase() === chosenPokemon.name) {
      setGameWon(true);
    }
    setIsGameOver(true);
  };

  useEffect(() => {
    if (gameWon) {
      setTimeout(() => {
        setGameWon(false);
      }, 3000);
    }
  }, [gameWon]);

  return (
    <>
      <div className="pokemonGuesserContainer">
        {chosenPokemonImage && !isGameOver ? (
          <img
            key="chosenPokemonImage"
            id="pokemon-image"
            className="hidden"
            alt="Mystery Pokemon"
            src={chosenPokemonImage}
          />
        ) : gameWon ? (
          <img
            key="chosenPokemonImage"
            id="pokemon-image"
            className="visible"
            alt={`${chosenPokemon.name}`}
            src={chosenPokemonImage}
          />
        ) : (
          <input
            className="pokemonGuesserButton"
            type="image"
            src={pokeball}
            alt="Get pokemon"
            onClick={handleReset}
          />
        )}
      </div>
      <div className="inputContainer">
        {loading ? (
          <p>Loading...</p>
        ) : pokemonList.length === 0 ? (
          <p>Click the pokeball and choose a Pokemon!</p>
        ) : isGameOver ? (
          gameWon ? (
            <p>You won!</p>
          ) : (
            <p> Try Again</p>
          )
        ) : (
          <div className="inputButtonContainer">
            {'Whos that Pokemon?'}
            {random4Pokemon &&
              random4Pokemon.map((pokemon) => (
                <>
                  <li>
                    <button key={pokemon.name} onClick={handlePokemonSelect}>
                      {pokemon.name.slice(0, 1).toUpperCase() +
                        pokemon.name.slice(1)}
                    </button>
                  </li>
                </>
              ))}
          </div>
        )}
        {error && <p>{error}</p>}
      </div>
    </>
  );
}

export default App;
